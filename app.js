const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = process.env.PORT || 3000;      
// wehn we work on Heroku, heroku wil generate a port for us. When I am working locally i.e. on my localhost, I will go here to 3000;
// PORT global variable heroku craetes beforewe delpoy the application
// when w ework with Heroku we still need to work locally on our localhost
// set the view engine to ejs
app.set("view engine", "ejs");

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => res.render("airbnb"));

app.listen(port, () => console.log(`Server started on port ${port}!`));

/*
app.get("/host/experiences?from_nav=1", function(req, res) {
	res.sendFile(path.join(__dirname + "/airbnb.html"));
});
*/

const location = "Rome";
const id = 122;
const rome = {
  name: "rome",
  homes: [
    {
      title: "Rome's Apartment",
      price: "12$",
      img_url:
        "https://a0.muscache.com/im/pictures/f5bd3f39-8162-46d1-a670-a39a832072e9.jpg?aki_policy=large",
      stars: "4 stars",
      description: "Very cool!"
    },
    {
      title: "Rome's Apartment",
      price: "12$",
      img_url:
        "https://a0.muscache.com/im/pictures/f5bd3f39-8162-46d1-a670-a39a832072e9.jpg?aki_policy=large",
      stars: "4 stars",
      description: "Very cool!"
    },
    {
      title: "Rome's Apartment",
      price: "12$",
      img_url:
        "https://a0.muscache.com/im/pictures/f5bd3f39-8162-46d1-a670-a39a832072e9.jpg?aki_policy=large",
      stars: "4 stars",
      description: "Very cool!"
    },
    {
      title: "Rome's Apartment",
      price: "12$",
      img_url:
        "https://a0.muscache.com/im/pictures/f5bd3f39-8162-46d1-a670-a39a832072e9.jpg?aki_policy=large",
      stars: "4 stars",
      description: "Very cool!"
    }
  ]
};

app.get("/s/:location/all/", (req, res) => {
  console.log(rome.homes);
  if (req.params.location === "rome") {
    res.render("location");
  } else {
    res.send("We found no results for that query");      // we are sending here this sentence "We found no results for that query" to the client that is why we used "send" and not "return"
  }
});

app.get("/s/:location/homes", (req, res) => res.render("homes", {apartments: rome.homes}));
app.get("/rooms/:id", (req, res) => res.render("rooms"));

app.get("/homes/new", (req, res) => res.render("new_home"));

app.post("/homes", (req, res) => console.log(req.body));

// /s/:location/all
// /s/:location/homes
// /rooms/:id


/* 404 */

app.get("*", function(req, res) {
  res.send("Something went wrong!", 404);
});
